import xml.etree.cElementTree as ET
import sqlite3
from sqlite3 import Error

class loginHandler:

    def create_connection():
        db = "sample.db"
        try:
            conn = sqlite3.connect(db)
            return conn
        except Error as e:
            print(e)
            return None


    def doLogin(self, u, p):
        print("LOGIN.....")
        if u is None or p is None:
            return -1, "Please enter both values"
        else:
            conn = loginHandler.create_connection()
            with conn:
                cur = conn.cursor()
                try:
                    cur.execute("SELECT * FROM SampleTable WHERE Username=? and Psw=?" , (u,p))
                    rows = cur.fetchall()

                    if not rows:
                        return -1, "Pease verify data"
                    else:
                        #here we could create the access token or whatever
                        return 1, "success"


                except Error as e:
                    return -2, "Error accessing DB"

            return -2, "Fatal error"
