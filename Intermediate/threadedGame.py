import threading
import logging

current_language=""
current_name=""
continue_language=True
continue_name=True
loop_language=0
loop_name=0
languages = ["C", "C++", "Perl", "Python", "Java", "Android", "C#", "Swift", "ObjectiveC", "PHP", "Ruby", "Node"]
devs = ["Peter", "Louis", "Brian", "Stuie", "Meg", "Chris", "Homer", "Marge", "Lisa", "Bart", "Magie"]
selected_language= ""
selected_dev= ""
most = "0"

class threadedGame:

    def func1():
        global current_language, continue_language, loop_language

        while continue_language:
            for x in languages:
                current_language = x
            loop_language=loop_language+1

    def func2():
        global current_name, continue_name, loop_name

        while continue_name:
            for x in devs:
                current_name = x
            loop_name=loop_name+1

    def func3():
        global continue_name, continue_language, current_name, current_language, loop_language, loop_name, selected_language, selected_dev, most
        print(selected_dev)
        print(selected_language)
        while True:
            if current_name==selected_dev and current_language==selected_language:
                continue_name=False
                continue_language=False
                print("*" * 60)
                print(current_name)
                print(current_language)
                print("Language Loops: ", loop_language)
                print("name Loops: ", loop_name)

                print(most)

                if most=="0":
                    if loop_name>loop_language:
                        print("win!" * 10)
                    else:
                        print("Loose!" * 10)
                elif most=="1":
                    if loop_name<loop_language:
                        print("win!" * 10)
                    else:
                        print("Loose!" * 10)
                else:
                    print(most + " was never an option, therefore you loose")

                break


    def main():
        global continue_name, continue_language, languages, devs, selected_language, selected_dev, most

        while True:
            print(languages)
            l = input("Select a language: ")
            if l in languages:
                selected_language=l
                break
            else:
                print("****Error****")

        while True:
            print(devs)
            d = input("Select a dev: ")
            if d in devs:
                selected_dev = d
                break
            else:
                print("****Error****")

        most = input("Which one will loop more? Devs or Languages? (0-devs 1-languages): ")

        continue_name=True
        continue_language=True

        t = threading.Thread(target=threadedGame.func1)
        t.setDaemon(True)
        t2 = threading.Thread(target=threadedGame.func2)
        t2.setDaemon(True)
        t3 = threading.Thread(target=threadedGame.func3)
        t3.setDaemon(True)

        t.start()
        t2.start()
        t3.start()



        main_thread = threading.current_thread()
        for t in threading.enumerate():
            if t is main_thread:
                continue
            t.join()
