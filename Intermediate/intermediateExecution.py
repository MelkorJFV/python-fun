#Python intermediate
from loginHandler import loginHandler
from threadedGame import threadedGame
keepAlive = True
isLogin = False

def exit():
    global keepAlive
    keepAlive = False

def login():
    global isLogin

    if not isLogin:
        print("Login Method")
        usr = input("Username: ")
        psw = input("Password: ")

        lh = loginHandler()
        code, message = lh.doLogin(usr, psw)
        if(code == -2):
            exit()
        elif(code == -1):
            print(message)
        else:
            print(message)
            isLogin = True

    else:
        print("Already logged In")

def play():
    global isLogin
    if isLogin:
        threadedGame.main()
    else:
        print("please Login to continue")

def runSubprocess():
    #The subprocess module allows you to spawn new processes,
    #connect to their input/output/error pipes, and obtain their return codes.
    #This module intends to replace several older modules and functions
    import subprocess
    subprocess.call(['python3','guiSample.py'])




if __name__ == '__main__':
    while keepAlive:
        print("*"*20)
        f = input("function? - ")
        #locals() bring up all functions
        #and variables in the environment
        if f in locals():
            func = locals()[f]
            func()
        else:
            print("Error")
