import threading
import logging



def func1():
    languages = ["C", "C++", "Perl", "Python", "Java", "Android", "C#", "Swift", "ObjectiveC", "PHP", "Ruby", "Node"]
    #languages = ["111"]
    for x in languages:
        print("Language: " + x)

def func2():
    devs = ["Peter", "Louis", "Brian", "Stuie", "Meg", "Chris", "Homer", "Marge", "Lisa", "Bart", "Magie"]
    #devs = ["222"]
    for x in devs:
        print("Name: " + x)



if __name__ == '__main__':

    t = threading.Thread(target=func1)
    t.setDaemon(True)
    t2 = threading.Thread(target=func2)
    t2.setDaemon(True)

    t.start()
    t2.start()


    main_thread = threading.current_thread()
    for t in threading.enumerate():
        if t is main_thread:
            continue
        print('------------joining: ', t.getName())
        t.join()
