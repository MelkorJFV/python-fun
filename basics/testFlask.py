"""Ejercicio 1"""
#Que retorna el siguiente ejemplo
#si se llama de la siguiente forma?



from flask import Flask, request, Response
import xml.etree.cElementTree as ET
app = Flask(__name__)

@app.route('/services', methods=['GET'])
def services():
    h_val = request.args.get('h')
    if h_val==1:
        return "Option1"
    else:
        return "Option2"

if __name__ == '__main__':
    app.run()
