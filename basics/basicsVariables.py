#Python basics
class BasicClass:
    #Static Class Variable
    bcv = "Basic Class Variable"

def auxFunction():
    #Variable de metodo
    afv = "Aux Function Variable"
    print(afv)

if __name__ == '__main__':
    #Variable
    a = "Variable a"

    print(a)
    auxFunction()
    print(BasicClass.bcv)

    #Class instance
    bc = BasicClass()
    print(bc.bcv)

    #Creating a non Static variable
    #that overrides Static Variable
    #ONLY for the instance
    bc.bcv = "changing value"

    print(bc.bcv)
    print(BasicClass.bcv)
