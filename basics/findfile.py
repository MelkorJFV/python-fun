import os
import fnmatch
import sys
from pathlib import Path

curr_dir = os.getcwd()

pattern = "*.py"
result = []
for root, dirs, files in os.walk(curr_dir):
    for name in files:
        if fnmatch.fnmatch(name, pattern):
            result.append(os.path.join(root, name))

for r in result:
    print(str(r))
