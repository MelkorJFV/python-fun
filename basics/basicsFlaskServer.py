from flask import Flask, request, Response
import xml.etree.cElementTree as ET
app = Flask(__name__)

@app.route('/')
def hello():
    return "Hello World!"

@app.route('/services', methods=['GET'])
def services():
    acc = request.args.get('acc')
    print(acc)

    root = ET.Element("info")
    status = ET.SubElement(root, "status")
    ET.SubElement(status, "code").text = "1"
    ET.SubElement(status, "text").text = "This is great"

    xmlText = ET.tostring(root, encoding='utf8', method='xml')
    print(xmlText)

    return Response(xmlText, mimetype='text/xml')


if __name__ == '__main__':
    app.run()
