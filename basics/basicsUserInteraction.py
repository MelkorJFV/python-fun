#Python basics
import datetime
name = input("Name? - ")
while True:
    ageInput = input("Age? - ")
    try:
        int(ageInput)
        age = int(ageInput)
        now = datetime.datetime.now()
        born = now.year - age
        print(name, "born: ", born)
        break
    except ValueError:
        print("please enter an int value.")
