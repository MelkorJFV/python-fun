"""Ejercicio 1"""
#Que se imprime en el
#siguiente ejemplo?
a=2
b=3
c="Hola"
if a==b:
    print("option 1")
elif a<3 and c=="hola":
    print("option 2")
else:
    print("option 3")

"""Ejercicio 2"""
#Que se imprime en el
#siguiente ejemplo?
arr = ('q','w','e','r','t','y')
for val in arr[:-2]:
    print(val)


"""Ejercicio 3"""
#Estos dos segmentos,
#imprimen lo mismo
#Segmento 1
print ("-" * 10)
#Segmento 2
a=0
while a<10:
    print('-')
    a=a+1
