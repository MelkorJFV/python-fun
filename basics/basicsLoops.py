#Python basics
#Print a variable 60 times
print ("-" * 60)

arr = (1,2,3,4,5,6,7,8)
for a in arr:
    print(a)
print ("-" * 60)
#print all except last element
for a in arr[:-1]:
    print(a)
print ("-" * 60)
#print only first 3 elements
for a in arr[:3]:
    print(a)
print ("-" * 60)

#While Loop
bool = True
i = 5
m = -5
while bool:
    print(i)
    i = i-1
    if i==0 or i ==-1:
        continue
    elif i<-2:
        break
    elif i<m:
        bool = False
    print("*" * 10)

print ("-" * 60)
